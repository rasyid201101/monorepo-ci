package com.mvpotter.gitlab.monorepo.maven;

import spark.Spark;

public class ServiceOneApplication {

    public static void main(String[] args) {
        Spark.port(9001);
        Spark.get("/", (req, res) -> "Hi, I am service-one");
    }

}
