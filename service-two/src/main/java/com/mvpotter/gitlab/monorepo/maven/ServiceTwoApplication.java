package com.mvpotter.gitlab.monorepo.maven;

import spark.Spark;

public class ServiceTwoApplication {

    public static void main(String[] args) {
        Spark.port(9002);
        Spark.get("/", (req, res) -> "Hi, I am service-two");
    }

}
